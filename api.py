from flask import Flask,render_template,request
app = Flask(__name__)
from predict_price import predict_appartment
@app.route('/')
def hello_world():
    return render_template('index.html')
@app.route('/get_predict',methods=['GET'])
def get_predict():
    data = request.args 
    direction = data["direction"]
    address_geo_location = data["address_geo_location"]
    address_geo_lat = float(data["address_geo_lat"])
    address_geo_long = float(data["address_geo_long"])
    balcony_direction = data["balcony_direction"]
    num_bedroom = int(data["num_bedroom"])
    num_toilet = int(data["num_toilet"])
    size = int(data["size"])
    return str(predict_appartment(address_geo_location,size,num_bedroom,num_toilet,balcony_direction,direction,address_geo_lat,address_geo_long))
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000)